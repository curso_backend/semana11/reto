from django.db import models
from django.utils import timezone


class Book(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.ForeignKey(
        'Category', null=False, blank=False, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    author = models.CharField(max_length=100, default='Anónimo')
    published_date = models.DateField(
        null=True, blank=True)
    registered_date = models.DateTimeField(
        default=timezone.now)
    updated_date = models.DateField(
        null=True, blank=True)
    
    def __str__(self):
        return f'{self.title} / {self.category}'

class Category(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.CharField(max_length=100)
    
    def __str__(self):
        return self.category

class Renting(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(
        'auth.User', null=False, blank=False, on_delete=models.CASCADE)
    book = models.ForeignKey(
        'Book', null=False, blank=False, on_delete=models.CASCADE)
    #price = models.FloatField(default=5)
    renting_date = models.DateTimeField(
        default=timezone.now)
    #renting_time = models.IntegerField()
    return_date = models.DateTimeField()
    """Obtener de Renting_Detail
    subtotal = models.FloatField()
    igv = int(renting_time) * 0.18
    total = int(subtotal) + igv"""
    
    def __str__(self):
        return f'Usuario: {self.user} / Libro: {self.book}, Fecha Devolucion: {self.return_date}'
"""
class Renting_Detail(models.Model):
    id = models.AutoField(primary_key=True)
    renting = models.ForeignKey(
        'Renting', null=True, blank=True, on_delete=models.CASCADE)
    book = models.ForeignKey(
        'Book', null=False, blank=False, on_delete=models.CASCADE)
    price = models.FloatField(default=5)

    def __str__(self):
        return f'{self.book} / {self.price}'
"""
